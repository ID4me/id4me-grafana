+++
title = "ID4me OIDC authentication"
description = "Grafana ID4me Guide "
keywords = ["grafana", "configuration", "documentation", "oauth", "id4me"]
weight = 1000
+++

# ID4me OIDC authentication

> Only available in Grafana v7.0+

The ID4me authentication allows your Grafana users to log in by using an external ID4me identity provider aka identity authority.

## Create an ID4me application

Before you can sign a user in, you need to register an ID4me ID on a corresponding ID4me identity authority.
Available ID4me related authorities are:

[Denic-ID](https://id.denic.de)


## Enable ID4me OIDC in Grafana

1. Add the following to the [Grafana configuration file]({{< relref "../administration/configuration.md#config-file-locations" >}}):

```bash
[auth.id4me]
name = ID4me
enabled = true
allow_sign_up = true
client_id = some_id
client_secret = some_secret
scopes = openid profile email
issuer_url =  https://<id4me-issuer>
auth_url = https://<id4me-issuer>/login
token_url = https://<id4me-issuer>/token
api_url = https://<id4me-issuer>/userinfo
allowed_domains =
allowed_groups =
email_attribute_path =
login_attribute_path =
name_attribute_path =
role_attribute_path =
```

### Configure allowed groups and domains

To limit access to authenticated users that are members of one or more groups, set `allowed_groups`
to a comma- or space-separated list.

```ini
allowed_groups = Developers, Admins
```

The `allowed_domains` option limits access to the users belonging to the specific domains. Domains should be separated by space or comma.

```ini
allowed_domains = mycompany.com mycompany.org
```

### Map roles

Grafana can attempt to do role mapping through ID4me authorization step. In order to achieve this, Grafana checks for the presence of a role using the [JMESPath](http://jmespath.org/examples.html) specified via the `role_attribute_path` configuration option.

Grafana uses JSON obtained from querying the `/userinfo` endpoint for the path lookup. The result after evaluating the `role_attribute_path` JMESPath expression needs to be a valid Grafana role, i.e. `Viewer`, `Editor` or `Admin`. Refer to [Organization roles]({{< relref "../permissions/organization_roles.md" >}}) for more information about roles and permissions in Grafana.

Read about how to on Generic OAuth page for [JMESPath examples]({{< relref "generic-oauth.md/#jmespath-examples" >}}).

### Map email, username and login

Besides roles Grafana can attempt to map email, username and login through ID4me authorization step, too. Therefore check [Generic OAuth page]({{< relref "generic-oauth.md/#generic-oauth-authentication" >}}).

